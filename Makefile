.PHONY: run

IMAGE=alanocallaghan/basicsworkflow2020-docker
VERSION=0.4.0

all: rds/chain_active.Rds rds/chain_SM.Rds

run:
	docker run -v $(shell pwd):/home/rstudio/mycode \
		-w /home/rstudio/mycode \
		-v /tmp/.X11-unix:/tmp/.X11-unix:ro \
		-e DISPLAY=${DISPLAY} \
		-u rstudio \
		-it $(IMAGE):$(VERSION) \
		/bin/bash

rds/chain_active.Rds: chains_CD4.R
	docker run -v $(shell pwd):/home/rstudio/mycode \
		-w /home/rstudio/mycode \
		$(IMAGE):$(VERSION) \
		/bin/bash \
		-c "Rscript chains_CD4.R"

rds/chain_SM.Rds: chains_mesoderm.R
	docker run -v $(shell pwd):/home/rstudio/mycode \
		-w /home/rstudio/mycode \
		$(IMAGE):$(VERSION) \
		/bin/bash \
		-c "Rscript chains_mesoderm.R"
