## ----setup_knitr, include = FALSE, cache = FALSE------------------------------
library("BiocStyle")
library("knitr")
knitr::opts_chunk$set(
  message = FALSE, error = FALSE, warning = FALSE,
  cache = 0, cache.path = "cache_supp/",
  fig.path = "figure_supp/"
)


## ----libs---------------------------------------------------------------------
library("SingleCellExperiment")
library("scater")
library("scran")
library("BASiCS")
library("ggplot2")
library("ggpointdensity")
library("biomaRt")
library("patchwork")
library("viridis")
## set default theme for plots
theme_set(theme_bw())


## ----download-droplet-data----------------------------------------------------
options(timeout = 1000)
# Check whether the data was downloaded already
if (!file.exists("downloads/rawCounts.tsv")) {
  website <- "https://www.ebi.ac.uk/"
  folder <- "biostudies/files/E-MTAB-6153/"
  download.file(
    paste0(website, folder),
    destfile = "downloads/E-MTAB-6153.zip"
  )
  unzip("downloads/E-MTAB-6153.zip", exdir = "downloads")
}


## ----load-droplet-data-raw----------------------------------------------------
raw_counts <- read.delim("downloads/rawCounts.tsv", header = TRUE)


## ----cluster-labels-droplet---------------------------------------------------
cluster_labels <- read.table("downloads/cellAnnotation.tsv",
  sep = "\t", header = TRUE, stringsAsFactors = FALSE)


## ----table-cluster-labels-droplet---------------------------------------------
table(cluster_labels$cellType, useNA = "ifany")


## ----table-subclusters--------------------------------------------------------
# Extract sub-type cell labels (sample sub-index is removed)
cluster_labels$subCellType <- sub("_.*", "", cluster_labels$cell)
table(cluster_labels$subCellType, cluster_labels$cellType)


## ----table-batch--------------------------------------------------------------
cluster_labels$sample <- as.factor(cluster_labels$sample)
table(cluster_labels$sample)


## ----droplet-cell-selection---------------------------------------------------
# Identifies which cells are in the groups of interest
ind_som <- which(
  cluster_labels$cellType == "somiticMesoderm" |
  cluster_labels$cellType == "presomiticMesoderm"
)
# Selects the cells of interest from the raw counts and cluster-label metadata
raw_counts <- raw_counts[, ind_som]
cluster_labels <- cluster_labels[ind_som, ]


## ----droplet-sce--------------------------------------------------------------
droplet_sce <- SingleCellExperiment(
  assays = list(counts = as(as.matrix(raw_counts), "dgCMatrix")),
  colData = cluster_labels
)
# Delete the original table of raw expression counts as no longer needed
rm(raw_counts)


## ----remove-counts------------------------------------------------------------
ind_expressed <- Matrix::rowMeans(counts(droplet_sce)) > 0.1
droplet_sce <- droplet_sce[ind_expressed, ]


## ----obtain-gene-symbols------------------------------------------------------
# Check if data storage folder exists and create it if needed
if (!dir.exists("rds")) {
  dir.create("rds", showWarnings = FALSE)
}
if (!file.exists("rds/genenames.Rds")) {
  # Initialize mart and dataset
  ensembl <- useEnsembl(
    biomart = "genes",
    version = 104,
    dataset = "mmusculus_gene_ensembl"
  )
  # Select gene ID and gene name
  genenames <- getBM(
    attributes = c("ensembl_gene_id", "external_gene_name", "gene_biotype"),
    mart = ensembl
  )
  rownames(genenames) <- genenames$ensembl_gene_id
  # Store information for future use
  saveRDS(genenames, "rds/genenames.Rds")
}
# Load pre-downloaded gene annotation information
genenames <- readRDS("rds/genenames.Rds")


## -----------------------------------------------------------------------------
## Merge biomaRt annotation
rowdata <- data.frame(ensembl_gene_id = rownames(droplet_sce))
rowdata <- merge(rowdata, genenames, by = "ensembl_gene_id", all.x = TRUE)
rownames(rowdata) <- rowdata$ensembl_gene_id
# Sort to match the order in the original data
rowdata <- rowdata[rownames(droplet_sce),]
## Check if  order is correct after merge;
stopifnot(all(rownames(rowdata) == rownames(droplet_sce)))
## add to the SingleCellExperiment object
rowData(droplet_sce) <- rowdata


## -----------------------------------------------------------------------------
ind_pc <- which(rowData(droplet_sce)$gene_biotype == "protein_coding")
droplet_sce <- droplet_sce[ind_pc, ]


## ----add-cellqc-metrics-------------------------------------------------------
droplet_sce <- addPerCellQC(droplet_sce)


## ----cellqc-metrics, fig.cap="Cell-level QC metrics. For each cell cluster: distribution of the total number of UMIs per cell (left) and the total number of detected genes per cell (right)."----
p_cell_qc1 <- plotColData(droplet_sce, y = "sum", x = "subCellType") 
p_cell_qc2 <- plotColData(droplet_sce, y = "detected", x = "subCellType")
p_cell_qc1 + p_cell_qc2


## ----drop-filt----------------------------------------------------------------
ind_retain <- colData(droplet_sce)$subCellType != "presomiticMesoderm.b" &
  colData(droplet_sce)$sum <= 25000
droplet_sce <- droplet_sce[, ind_retain]


## ----pca-visualisation-mesoderm-batch, fig.cap="First two principal components of log-transformed expression counts after scran normalisation. Colour indicates the experimental condition (left), the number of detected genes (middle) and animal of origin (right) for each cell."----
## Global scaling normalisation 
droplet_sce <- computeSumFactors(droplet_sce, colData(droplet_sce)$subCellType)
## Obtain log-normalised expression counts
droplet_sce <- logNormCounts(droplet_sce)
## Run PCA
droplet_sce <- runPCA(droplet_sce)
## Plot first two PCs colour-coded according to cell-level metadata
p_type <- plotPCA(
    droplet_sce,
    ncomponents = c(2, 1),
    colour_by = "subCellType"
  ) +
  theme(legend.position = "bottom")
p_detected <- plotPCA(
    droplet_sce,
    ncomponents = c(2, 1),
    colour_by = "detected"
  ) +
  theme(legend.position = "bottom")
p_batch <- plotPCA(
    droplet_sce,
    ncomponents = c(2, 1),
    colour_by = "sample"
  ) +
  theme(legend.position = "bottom")
p_type + p_detected  + p_batch 


## ----gene-selection, fig.cap="Average UMI-count for each gene is plotted against the number of cells in which that gene was detected. Dashed grey lines are shown at the thresholds below which genes are removed."----
## Calculate gene-level QC metrics using scater
droplet_sce <- addPerFeatureQC(droplet_sce, exprs_values = "counts")
## Remove genes with zero total counts across all cells
droplet_sce <- droplet_sce[rowData(droplet_sce)$detected != 0, ]

## Transform "detected" into number of cells and define inclusion criteria
rowData(droplet_sce)$detected_cells <-
  rowData(droplet_sce)$detected * ncol(droplet_sce) / 100

## Set detection threshold to detecting expression in at least 20 cells
detected_threshold <- 20
rowData(droplet_sce)$include_gene <- 
  rowData(droplet_sce)$detected_cells >= detected_threshold

## Plot gene-level metrics, highlighting those included for our analysis
plotRowData(droplet_sce,
    x = "detected_cells",
    y = "mean",
    colour_by = "include_gene"
  ) +
  xlab("Number of cells in which expression was detected") +
  ylab("Average number of read counts across all cells") +
  scale_x_log10() +
  scale_y_log10() +
  theme(legend.position = "bottom") +
  geom_vline(
    xintercept = detected_threshold,
    linetype = "dashed",
    col = "grey60"
  )

## Apply gene filter
droplet_sce <- droplet_sce[rowData(droplet_sce)$include_gene, ]


## ----separate-SCE-------------------------------------------------------------
sce_psm <- droplet_sce[, colData(droplet_sce)$cellType == "presomiticMesoderm"]
sce_sm <- droplet_sce[, colData(droplet_sce)$cellType == "somiticMesoderm"]


## -----------------------------------------------------------------------------
saveRDS(sce_psm, "rds/sce_psm.rds")
saveRDS(sce_sm, "rds/sce_sm.rds")

## ----setup_knitr, include = FALSE, cache = FALSE------------------------------
library("BiocStyle")
library("knitr")



## ----libs---------------------------------------------------------------------
library("SingleCellExperiment")
library("scater")
library("scran")
library("BASiCS")
library("ggplot2")
library("ggpointdensity")
library("biomaRt")
library("patchwork")
library("viridis")
## set default theme for plots
theme_set(theme_bw())


## ----download-droplet-data----------------------------------------------------
options(timeout = 1000)
# Check whether the data was downloaded already
if (!file.exists("downloads/rawCounts.tsv")) {
  website <- "https://www.ebi.ac.uk/"
  folder <- "biostudies/files/E-MTAB-6153/"
  download.file(
    paste0(website, folder),
    destfile = "downloads/E-MTAB-6153.zip"
  )
  unzip("downloads/E-MTAB-6153.zip", exdir = "downloads")
}


## ----load-droplet-data-raw----------------------------------------------------
raw_counts <- read.delim("downloads/rawCounts.tsv", header = TRUE)


## ----cluster-labels-droplet---------------------------------------------------
cluster_labels <- read.table("downloads/cellAnnotation.tsv",
  sep = "\t", header = TRUE, stringsAsFactors = FALSE)


## ----table-cluster-labels-droplet---------------------------------------------
table(cluster_labels$cellType, useNA = "ifany")


## ----table-subclusters--------------------------------------------------------
# Extract sub-type cell labels (sample sub-index is removed)
cluster_labels$subCellType <- sub("_.*", "", cluster_labels$cell)
table(cluster_labels$subCellType, cluster_labels$cellType)


## ----table-batch--------------------------------------------------------------
cluster_labels$sample <- as.factor(cluster_labels$sample)
table(cluster_labels$sample)


## ----droplet-cell-selection---------------------------------------------------
# Identifies which cells are in the groups of interest
ind_som <- which(
  cluster_labels$cellType == "somiticMesoderm" |
  cluster_labels$cellType == "presomiticMesoderm"
)
# Selects the cells of interest from the raw counts and cluster-label metadata
raw_counts <- raw_counts[, ind_som]
cluster_labels <- cluster_labels[ind_som, ]


## ----droplet-sce--------------------------------------------------------------
droplet_sce <- SingleCellExperiment(
  assays = list(counts = as(as.matrix(raw_counts), "dgCMatrix")),
  colData = cluster_labels
)
# Delete the original table of raw expression counts as no longer needed
rm(raw_counts)

ind_expressed <- Matrix::rowMeans(counts(droplet_sce)) > 0.1
droplet_sce <- droplet_sce[ind_expressed, ]

## ----obtain-gene-symbols------------------------------------------------------
# Check if data storage folder exists and create it if needed
if (!dir.exists("rds")) {
  dir.create("rds", showWarnings = FALSE)
}
if (!file.exists("rds/genenames.rds")) {
  # Initialize mart and dataset
  ensembl <- useEnsembl(
    biomart = "genes",
    version = 104,
    dataset = "mmusculus_gene_ensembl"
  )
  # Select gene ID and gene name
  genenames <- getBM(
    attributes = c("ensembl_gene_id", "external_gene_name", "gene_biotype"),
    mart = ensembl
  )
  rownames(genenames) <- genenames$ensembl_gene_id
  # Store information for future use
  saveRDS(genenames, "rds/genenames.rds")
}
# Load pre-downloaded gene annotation information
genenames <- readRDS("rds/genenames.rds")


## -----------------------------------------------------------------------------
## Merge biomaRt annotation
rowdata <- data.frame(ensembl_gene_id = rownames(droplet_sce))
rowdata <- merge(rowdata, genenames, by = "ensembl_gene_id", all.x = TRUE)
rownames(rowdata) <- rowdata$ensembl_gene_id
# Sort to match the order in the original data
rowdata <- rowdata[rownames(droplet_sce),]
## Check if  order is correct after merge;
stopifnot(all(rownames(rowdata) == rownames(droplet_sce)))
## add to the SingleCellExperiment object
rowData(droplet_sce) <- rowdata


## -----------------------------------------------------------------------------
ind_pc <- which(rowData(droplet_sce)$gene_biotype == "protein_coding")
droplet_sce <- droplet_sce[ind_pc, ]


## ----add-cellqc-metrics-------------------------------------------------------
droplet_sce <- addPerCellQC(droplet_sce)


## ----cellqc-metrics, fig.cap="Cell-level QC metrics. For each cell cluster: distribution of the total number of UMIs per cell (left) and the total number of detected genes per cell (right)."----
p_cell_qc1 <- plotColData(droplet_sce, y = "sum", x = "subCellType") 
p_cell_qc2 <- plotColData(droplet_sce, y = "detected", x = "subCellType")
p_cell_qc1 + p_cell_qc2


## ----drop-filt----------------------------------------------------------------
ind_retain <- colData(droplet_sce)$subCellType != "presomiticMesoderm.b" &
  colData(droplet_sce)$sum <= 25000
droplet_sce <- droplet_sce[, ind_retain]


## ----pca-visualisation-mesoderm-batch, fig.cap="First two principal components of log-transformed expression counts after scran normalisation. Colour indicates the experimental condition (left), the number of detected genes (middle) and animal of origin (right) for each cell."----
## Global scaling normalisation 
droplet_sce <- computeSumFactors(droplet_sce, colData(droplet_sce)$subCellType)
## Obtain log-normalised expression counts
droplet_sce <- logNormCounts(droplet_sce)
## Run PCA
droplet_sce <- runPCA(droplet_sce)
## Plot first two PCs colour-coded according to cell-level metadata
p_type <- plotPCA(
    droplet_sce,
    ncomponents = c(2, 1),
    colour_by = "subCellType"
  ) +
  theme(legend.position = "bottom")
p_detected <- plotPCA(
    droplet_sce,
    ncomponents = c(2, 1),
    colour_by = "detected"
  ) +
  theme(legend.position = "bottom")
p_batch <- plotPCA(
    droplet_sce,
    ncomponents = c(2, 1),
    colour_by = "sample"
  ) +
  theme(legend.position = "bottom")
p_type + p_detected  + p_batch 


## ----gene-selection, fig.cap="Average UMI-count for each gene is plotted against the number of cells in which that gene was detected. Dashed grey lines are shown at the thresholds below which genes are removed."----
## Calculate gene-level QC metrics using scater
droplet_sce <- addPerFeatureQC(droplet_sce, exprs_values = "counts")
## Remove genes with zero total counts across all cells
droplet_sce <- droplet_sce[rowData(droplet_sce)$detected != 0, ]

## Transform "detected" into number of cells and define inclusion criteria
rowData(droplet_sce)$detected_cells <-
  rowData(droplet_sce)$detected * ncol(droplet_sce) / 100

## Set detection threshold to detecting expression in at least 20 cells
detected_threshold <- 20
rowData(droplet_sce)$include_gene <- 
  rowData(droplet_sce)$detected_cells >= detected_threshold

## Plot gene-level metrics, highlighting those included for our analysis
plotRowData(droplet_sce,
    x = "detected_cells",
    y = "mean",
    colour_by = "include_gene"
  ) +
  xlab("Number of cells in which expression was detected") +
  ylab("Average number of read counts across all cells") +
  scale_x_log10() +
  scale_y_log10() +
  theme(legend.position = "bottom") +
  geom_vline(
    xintercept = detected_threshold,
    linetype = "dashed",
    col = "grey60"
  )

## Apply gene filter
droplet_sce <- droplet_sce[rowData(droplet_sce)$include_gene, ]


## ----separate-SCE-------------------------------------------------------------
sce_psm <- droplet_sce[, colData(droplet_sce)$cellType == "presomiticMesoderm"]
sce_sm <- droplet_sce[, colData(droplet_sce)$cellType == "somiticMesoderm"]


## -----------------------------------------------------------------------------
saveRDS(sce_psm, "rds/sce_psm.Rds")
saveRDS(sce_sm, "rds/sce_sm.Rds")

set.seed(42)
t0 <- proc.time()
chain_sm <- BASiCS_MCMC(
  Data = sce_sm,
  N = 20000,
  Thin = 10,
  Burn = 10000,
  Regression = TRUE,
  WithSpikes = FALSE,
  PriorParam = BASiCS_PriorParam(sce_sm, PriorMu = "EmpiricalBayes"),
  Threads = 4,
  StoreChains = TRUE,
  StoreDir = "rds/",
  RunName = "SM"
)
t1 <- proc.time()
set.seed(43)
chain_psm <- BASiCS_MCMC(
  Data = sce_psm,
  N = 20000,
  Thin = 10,
  Burn = 10000,
  Regression = TRUE,
  WithSpikes = FALSE,
  PriorParam = BASiCS_PriorParam(sce_psm, PriorMu = "EmpiricalBayes"),
  Threads = 4,
  StoreChains = TRUE,
  StoreDir = "rds/",
  RunName = "PSM"
)
t2 <- proc.time()

saveRDS(list(t0, t1, t2), "rds/times_mesoderm.Rds")
